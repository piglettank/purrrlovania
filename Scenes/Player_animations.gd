extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var MOVE_SPEED = 75
export var PLAYER_GRAVITY = 5
export var JUMP_FORCE = 250
var animation_machine 


# var b = "text"
var direction: int
var velocity: Vector2
var wants_to_jump: bool
var is_touching_floor: bool

# Called when the node enters the scene tree for the first time.
func _process(delta):
	var label = $Label
	animation_machine = $AnimationTree.get("parameters/playback")
	label.text = String(wants_to_jump)
	_get_inputs()
	_get_jump_reqs()
	
	velocity += Vector2.DOWN * PLAYER_GRAVITY
	
	if(wants_to_jump && is_touching_floor):
		velocity += Vector2.UP * JUMP_FORCE
	
	velocity.x = MOVE_SPEED * direction
	
	velocity = move_and_slide(velocity, Vector2.UP) 
	
#Animation machine, I guess this is the way you're not supposed to do it. I just wanted to try it out

	if(Input.is_action_just_pressed("input_right") && $player_sprite.scale.x > 0):
		$player_sprite.scale.x *= -1
	
	if(Input.is_action_just_pressed("input_left") && $player_sprite.scale.x < 0):
		$player_sprite.scale.x *= -1
	
	if(is_touching_floor != true && velocity.y > 0):
		animation_machine.travel("fall")
	
	if(is_touching_floor != true && velocity.y < 0):
		animation_machine.travel("jump")
		
	if(is_touching_floor == true):
		animation_machine.travel("run")
		
	$Label.text = ("Direction: %d" % velocity.x)
	
	pass


func _get_inputs():
	wants_to_jump = Input.is_action_just_pressed("input_jump")
	direction = int(Input.is_action_pressed("input_right")) - int(Input.is_action_pressed("input_left"))
	
	
func _get_jump_reqs():
	is_touching_floor = is_on_floor()
	
enum States{
	NONE,
	IDLE,
	WALK,
	JUMP,
	FALL
}
