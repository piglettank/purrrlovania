extends KinematicBody2D


export var MOVE_SPEED = 300
export var PLAYER_GRAVITY = 8
export var PLAYER_GRAVITY_MULT = 2
export var MAX_JUMP_FORCE = 350
export var MIN_JUMP_FORCE = 250
export var PLAYER_GRAVITY_BOX = Vector2(-60,100)
export var VOOBY_GRAVITY = 10

onready var animation = $AnimatedSprite


var UP = Vector2.UP
var DOWN = Vector2.DOWN
var input_direction: int
var direction: int
var velocity: Vector2
var wants_to_jump: bool
var is_touching_floor: bool
var player_state = PlayerStates.IDLE
var timer = 0
var ass = 0
var test_grav = Vector2.ZERO

func _process(delta):
	var label = $Label
	var label2 = $Label2
	label2.text = String(test_grav)
	label.text = String(ass)
	_get_inputs()
	_get_jump_reqs()
	_set_player_state()
	
	_do_gravity_stuff()
	
	
	_jump_stuff()
	
		
		
	velocity.x = MOVE_SPEED * input_direction
	
	velocity = move_and_slide(velocity, UP) 
	
	_set_player_animation()
	
	pass

func _do_gravity_stuff():
	
	var voobi_distance = (abs(PLAYER_GRAVITY_BOX.x) + abs(PLAYER_GRAVITY_BOX.y))/2
	ass = 0
	if !is_touching_floor && velocity.y > PLAYER_GRAVITY_BOX.x &&  velocity.y < PLAYER_GRAVITY_BOX.y:
		var voobi_diff = voobi_distance - abs(velocity.y)
		
		
		ass = 1 - (voobi_diff/ voobi_distance)
	
	
	test_grav = DOWN *  (PLAYER_GRAVITY + (PLAYER_GRAVITY * ass * VOOBY_GRAVITY))
	
	velocity += DOWN *  (PLAYER_GRAVITY + (PLAYER_GRAVITY * ass * VOOBY_GRAVITY))
	
	
	
	
	
func _jump_stuff():
	if(wants_to_jump && is_touching_floor):
		velocity = UP * MAX_JUMP_FORCE
	if(Input.is_action_just_released("input_jump") && velocity.y < -MIN_JUMP_FORCE):
		
		velocity.y = -MIN_JUMP_FORCE
		
	if velocity.y > 0 && velocity.y < 10:
		velocity.y *= 2



func _get_inputs():
	wants_to_jump = Input.is_action_just_pressed("input_jump")
	input_direction = int(Input.is_action_pressed("input_right")) - int(Input.is_action_pressed("input_left"))
	if(input_direction !=0):
		direction = input_direction
	
func _get_jump_reqs():
	is_touching_floor = is_on_floor()
	
	
func _set_player_animation():
	
	animation.flip_h = direction > 0
	match player_state:
		PlayerStates.IDLE:
			animation.play("IDLE")
		PlayerStates.WALK:	
			animation.play("WALK")
		PlayerStates.JUMP:
			animation.play("JUMP")
		PlayerStates.FALL:
			animation.play("FALL")
	pass
	
	
	
func _set_player_state():
	
	
	
	if(abs(velocity.x) > 0.1):
		player_state = PlayerStates.WALK
	else:
		player_state = PlayerStates.IDLE
	if(!is_touching_floor):
		if (velocity.y <= 0):
			player_state = PlayerStates.JUMP
		else:
			player_state = PlayerStates.FALL
	
		
	

enum PlayerStates{
	IDLE,
	WALK,
	JUMP,
	FALL
}
